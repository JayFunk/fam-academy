Rails.application.routes.draw do

  devise_for :users, controllers: {registrations: "registrations", omniauth_callbacks: 'users/omniauth_callbacks'}

  #########################
  ####
  # Public routes
  ####
  #########################

  root 'public#index'

  get '/articles', to: 'public#articles', as: 'articles'
  get '/news', to: 'public#news', as: 'news'

  get '/photo_albums', to: 'public#photo_albums', as: 'photo_albums'
  get '/video_albums', to: 'public#video_albums', as: 'video_albums'
  get '/album/:id', to: 'public#album', as: 'album'

  get '/finish_registration', to: 'public#finish_registration', as: 'finish_registration'
  # put '/complite_sign_up/:id', to: 'public#complite_sign_up', as: 'complite_sign_up'
  patch '/complite_sign_up/:id', to: 'public#complite_sign_up', as: 'complite_sign_up'

  ###
  # Static pages
  ###

  get '/marriage', to: 'public#marriage', as: 'marriage'
  get '/slav-bazar', to: 'public#slavBazar', as: 'slav-bazar'
  get '/krasnaya-gorka', to: 'public#krasnayaGorka', as: 'krasnaya_gorka'
  get '/rody', to: 'public#rody', as: 'rody'
  get '/vsem_mirom', to: 'public#support', as: 'support'

  #########################
  ####
  # Admin routes
  ####
  #########################

  resources :videos, except: [:index, :show, :new, :edit]
  get 'admin', to: 'admin#dashboard', as: 'admin'
  namespace :admin do
    resources :articles
    resources :users
    resources :video_albums
    resources :photo_albums
    resources :masters
    resources :members
  end

  ###
  # API
  ###

  get '/videoInfo', to: 'videos#getVideoInfo', as: 'getVideoInfo'
  get 'api/get_image_info', to: 'api#get_image_info'
  post 'api/upload_image', to: 'api#upload_image'
  put 'api/change_order', to: 'api#change_order'
  put 'api/change_archive', to: 'api#change_archive', as: 'change_archive'
  put 'api/archive_image', to: 'api#archive_image'
  put 'api/main_image', to: 'api#main_image', as: 'main_image'
  put 'api/bg_image', to: 'api#bg_image', as: 'bg_image'
  put 'api/change_image_info', to: 'api#change_image_info', as: 'change_image_info'
  delete 'api/delete_image', to: 'api#delete_image', as: 'delete_image'
  put 'api/add_member', to: 'api#add_member'
  put 'api/change_payment_method', to: 'api#change_payment_method'
  put 'api/create_payment', to: 'api#create_payment'

  get '/robots.txt' => 'application#robots'
  get 'sitemap.xml', :to => 'sitemap#index', :defaults => { :format => 'xml' }

  ###
  # Posts
  ###

  get '/:id', to: 'public#post', as: 'post'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
