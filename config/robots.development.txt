# See http://www.robotstxt.org/robotstxt.html for documentation on how to use the robots.txt file
User-agent: *
Sitemap: https://fam-academy.ru/sitemap.xml
Host: https://fam-academy.ru
Allow: /
Disallow: /admin
Disallow: /users
Disallow: /api
Disallow: /videoInfo
Disallow: /videos
