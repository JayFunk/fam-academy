class ApplicationController < ActionController::Base
  before_action :redirect_subdomain
  protect_from_forgery with: :exception

  def index

  end

  # Robots.txt
  def robots
    robots = File.read(Rails.root.join('config', "robots.#{Rails.env}.txt"))
    render plain: robots
  end

  def after_sign_in_path_for(resource)
      if resource.fakeEmail
        finish_registration_path
      else
        root_path
      end
  end

  def redirect_subdomain
    if request.host == 'www.fam-academy.ru'
      redirect_to 'https://fam-academy.ru' + request.fullpath, :status => 301
    end
  end

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = 'У вас недостаточно прав, на совершение этого действия'
    redirect_back(fallback_location: root_path)
  end
end
