class AdminController < ApplicationController
  load_and_authorize_resource
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :check_admin

  def dashboard

  end

  def check_admin
    if current_user && (current_user.role == 'user')
      flash[:error] = 'У вас недостаточно прав, на совершение этого действия'
      redirect_back(fallback_location: root_path)
    end
  end
end
