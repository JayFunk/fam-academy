class ApiController < ApplicationController
  protect_from_forgery prepend: true

  def upload_image
    p 'upload Image call'
    if params[:origin] == 'redactor'
      image = Image.new(image: params[:file][0])
      if image.save!
        if image.image.file.extension.downcase == "gif"
          render json: {file: {url: image.image.url, id: image.id}}
        else
          render json: {file: {url: image.image.medium.url, id: image.id}}
        end
      end
    else
      p 'Image Get'
      @image = Image.new(image_params)
      respond_to do |format|
        if @image.save
          format.json { render json: @image, status: :created}
        else
          format.json { render json: @image.errors, status: :unprocessable_entity }
        end
      end
    end

  end

  def change_order
    @data = params[:images] || params[:videos]
    @data.each_with_index do |x, index|
      obj = if params[:images]
        Image.find(x)
      elsif params[:videos]
        Video.find(x)
      end
      obj.order = index
      obj.save
    end
    respond_to do |format|
      format.json { render json: @data}
    end
  end

  def main_image
    @image = Image.find(params[:image])
    @image.photo_album.images.update_all(main_image: false) if @image.photo_album.present?
    @image.main_image = true
    @image.save
    respond_to do |format|
      if @image.save
        format.json { render json: @image, status: :ok}
      else
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete_image
    @image = Image.find(params[:image])

    respond_to do |format|
      if @image.destroy
        format.json { render json: {text: 'delete success'}, status: :ok}
      else
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  def change_image_info
    @image = Image.find(params[:image])
    if params[:photographerName] == ''
      @image.author = params[:photographerId]
      @image.author_register = true
    else
      @image.author = params[:photographerName]
      @image.author_register = false
    end
    @image.desc = params[:desc]

    respond_to do |format|
      if @image.save
        format.json { render json: @image, status: :ok}
      else
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  def get_image_info
    @image = Image.find(params[:image])

    respond_to do |format|
      format.json { render json: @image}
    end
  end

  def archive_image
    @image = Image.find(params[:image])

    unless @image.main_image
      if @image.archive
        @image.archive = false
      else
        @image.archive = true
      end
      respond_to do |format|
        if @image.save
          format.json { render json: @image, status: :ok}
        else
          format.json { render json: @image.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.json { render json: {status: 'main_image',text: 'Картинка не может быть помещена в архив, так как она выбрана главной для альбома'}}
      end
    end
  end

  def change_archive
    @content =
      if params[:type] == 'article'
        Admin::Article.find(params[:content])
      end
    if @content.archive
      @content.update(archive: false)
      respond_to do |format|
        format.json { render json: {status: false, text: 'Пост опубликован'}}
      end
    else
      @content.update(archive: true)
      respond_to do |format|
        format.json { render json: {status: true, text: 'Пост добавлен в архив'}}
      end
    end
  end

  def add_member
    member = Admin::Member.new({
      first_name: params[:first_name],
      last_name: params[:last_name],
      patronymic: params[:patronymic],
      phone: params[:phone],
      email: params[:email],
      birthday: params[:birthday],
      passport_series: params[:passport_series],
      passport_number: params[:passport_number],
      passport_issued: params[:passport_issued],
      address: params[:address],
      social_link: params[:social_link]
    })
    if member.save
      respond_to do |format|
        format.json { render json: {status: 'success', id: member.id}}
      end
    else
      respond_to do |format|
        format.json { render json: {status: 'error'}}
      end
    end
  end

  def change_payment_method
    member = Admin::Member.find(params[:member])
    member.payment_method = params[:payment_method]
    if member.update
      respond_to do |format|
        format.json { render json: {status: 'success'}}
      end
    else
      respond_to do |format|
        format.json { render json: {status: 'error'}}
      end
    end
  end

  def create_payment
    payment = Admin::Payment.new({
      member_id: params[:member],
      sum: params[:sum],
      date: params[:payment_date],
      method: params[:payment_method],
      type: params[:payment_type]
    })
    if payment.save
      # Admin::Member.find(params[:member]).set_status
      respond_to do |format|
        format.json { render json: {status: 'success'}}
      end
    else
      respond_to do |format|
        format.json { render json: {status: 'error'}}
      end
    end
  end


  private

    def image_params
      params[:image] = params[:file]
      params.permit(:image, :photo_album, :master)
    end

    def video_params
      params.permit(:title, :video_album_id, :link)
    end
end
