class PublicController < ApplicationController
  protect_from_forgery with: :exception

  def index
    @headData = {
      'desc' => 'Мы помогаем настраивать и поддерживать семейное благополучие на всех этапах развития семьи, очно для тюменцев и по скайпу для всего русскоязычного мира.',
      'meta_keywords' => 'Академия Семейного Благополучия',
      'url' => 'https://fam-academy.ru',
      'image' => ActionController::Base.helpers.image_url('bg.jpg')
    }
  end

  def articles
    @articles = Admin::Article.where(type: 'article').where(archive: false).order_by(:updated_at => 'desc')
  end

  def news
    @news = Admin::Article.where(type: 'news').where(archive: false)
  end

  def post
    @post = Admin::Article.find(params[:id])
    @headData = {
      'title' => @post.title + ' | Академия Семейного Благополучия',
      'desc' => @post.desc,
      'meta_keywords' => 'Академия Семейного Благополучия',
      'url' => "#{request.protocol}#{request.host_with_port}#{post_path(@post)}",
      'image' => "#{request.protocol}#{request.host_with_port}#{@post.header.medium.url}",
      'type' => 'article',
      'created_at' => @post.created_at,
      'updated_at' => @post.updated_at
    }
  end

  def photo_albums
    @photo_albums = Admin::PhotoAlbum.where(archive: false)
  end

  def video_albums
    @video_albums = Admin::VideoAlbum.where(archive: false)
  end

  def photo_album
    @album = Admin::PhotoAlbum.find(params[:id])
  end

  def video_album
    @album = Admin::VideoAlbum.find(params[:id])
  end

  # Static pages

  def marriage
    @headData = {
      'title' => 'Свадьбы в славянском стиле',
      'desc' => 'Свадьба - одно из важнейших событий в жизни каждого человека. Поэтому провести её необходимо красочно и незабываемо. Наши мастера помогут вам сделать это! Мы организуем, помогаем и проводим свадебные церемонии в славянском стиле с сохранением всех традиция и обрядов.',
      'meta_keywords' => 'Свадьба, Славянская свадьба, свадьба в славянском стиле, сказочная свадьба, русская казачья свадьба, ведрусская свадьба, организация свадьбы в славянском стиле, организация свадьбы с обрядами, свадьба с обрядами, свадебные ритуалы, поощь в организации свадьбы, помощь в проведении свадьбы',
      'url' => 'https://fam-academy.ru'
    }
  end

  def slavBazar
    @headData = {
      'title' => 'Славянский базар 2018',
      'desc' => 'В конце марта в Тюмени пройдет всероссийский фестиваль русской народной культуры «Славянский базар - 2018». В рамках двухдневного праздника запланированы более 150 мероприятий, начиная с выступлений творческих коллективов, заканчивая играми и показами традиционных костюмов. Здесь же состоятся множество мастер-классов, которые понравятся и взрослым, и малышам. Чего только стоит МК по ткачеству, где каждый внесет свою лепту в создание длинного-предлинного половика! Кроме того, горожане посетят множество полезных лекций и семинаров, приобретут безвредные продукты и сладости.',
      'meta_keywords' => 'Славянский базар, Славянский базар 2018, Фестиваль в Тюмени, Тюменский фестиваль, славянский фестиваль, Славянский фетиваль в Тюмени, фестиваль весеннего равноденствия, участие в фестивале, влонтёрство на фестивале, волонтёрство, Выставки, выставки на фестивале, Песни о Руси, Славянский стиль сегодня, Прописные истины, Здесь русский дух здесь Русью пахнет, одежды в славянском стиле, Славянская одежда, Заватская Марина, Елена Белоусова, Елена Дыкова, Аня Заватская, Ника Недолужко, Вероника Минец, Ольга Вараксина, Анна Ваганова, Татьяна Клыкова, Марина Тимофеева, Наталья Деева, Ольга Вараксина, Марина Васючкова, Светлана Григорьева, Ольга Отраднова, Елена Осипова, Олеся Такменина, Славянский базар Тюмень,',
      'url' => 'https://fam-academy.ru/slav-bazar',
      'image' => ActionController::Base.helpers.image_url('landings/slavBazar/header.jpg')
    }
  end

  def krasnayaGorka
    @headData = {
      'title' => 'Красная горка 2018',
      'desc' => 'Красная горка – душевный праздник победы весны над зимой, первое общее гулянье потеплу, по оттаявшей земле! Первые встречи с землёй, начинающей пробуждаться, с растениями, в которых начинают шевелиться соки жизни, с друзьями-знакомыми-роднёй, охмелевшими от первого солнца, весеннего воздуха, открывающихся просторов и перспектив.',
      'meta_keywords' => 'Академия Семейного Благополучия, Красная горка, красная горка Тюмень, Весна 2018, Тюмень паска 2018, Мероприятия к паскхе, Пасха, Технопарк Тюмень',
      'url' => 'https://fam-academy.ru/krasnaya-gorka',
      'image' => ActionController::Base.helpers.image_url('landings/krasnayaGorka/header.jpg')
    }
  end

  def rody
    @headData = {
      'title' => 'Центр Eстественного Рождения',
      'desc' => 'Одно из важных направлений работы Центра – формирование перинатальной культуры населения, соответствующей современным требованиям и рекомендациям; повышение осведомленности в вопросах подготовки и протекания беременности, особенностях послеродового периода. Такие мероприятия имеют большое значение для формирования здорового образа жизни, для охраны и укрепления здоровья мамы и ребенка.',
      'meta_keywords' => 'Центр Eстественного Рождения, Естественные роды, Роды Тюмень, Роды в Тюмени на дому, Роды на дому, Сопровождение беременности, Женская консултация, Традиционные роды',
      'url' => 'https://fam-academy.ru/rody',
      'image' => ActionController::Base.helpers.image_url('landings/rody/header.jpg')
    }
  end

  def support
    @headData = {
      'title' => 'Русское Общественное Движение «Всем миром!»',
      'desc' => 'Мы часто слышим от вас, что нужно сделать такие праздники по-настоящему регулярными, чтобы каждое солнечное событие (солнцестояния и равноденствия) было поддержано нами на Земле, чтобы добрая сила традиционной культуры помогала нам налаживать наши земные дела и отношения на самых разных уровнях. Мы тоже за это! Поэтому мы объявляем о создании Русского Общественного Движения «Всем миром!»! ',
      'meta_keywords' => 'РОД «Всем миром!», Славянский базар, Академия Семейного Благополучия, Фестивали тюмень, Организация фестивалей Тюмень',
      'url' => 'https://fam-academy.ru/vsem_mirom',
      'image' => ActionController::Base.helpers.image_url('landings/vsem_mirom/header.jpg')
    }
  end

  def finish_registration
    @user = current_user
  end

  def complite_sign_up
    @user = current_user
    respond_to do |format|
      if @user.update(user_params)
        @user.update(fakeEmail: false)
        format.html { redirect_to root_path, notice: 'Данные пользователя обновлены' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render finish_registration_path }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :surname, :writer, :photographer, :email)
    end
end
