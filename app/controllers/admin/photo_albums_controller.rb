class Admin::PhotoAlbumsController < AdminController
  before_action :set_admin_photo_album, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  # GET /admin/photo_albums
  # GET /admin/photo_albums.json
  def index
    @admin_photo_albums = Admin::PhotoAlbum.all
  end

  # GET /admin/photo_albums/1
  # GET /admin/photo_albums/1.json
  def show
  end

  # GET /admin/photo_albums/new
  def new
    @admin_photo_album = Admin::PhotoAlbum.new
  end

  # GET /admin/photo_albums/1/edit
  def edit
    @images = @admin_photo_album.images.order_by(:order => 'asc')
  end

  # POST /admin/photo_albums
  # POST /admin/photo_albums.json
  def create
    @admin_photo_album = Admin::PhotoAlbum.new(resource_params)

    respond_to do |format|
      if @admin_photo_album.save
        format.html { redirect_to @admin_photo_album, notice: 'Photo album was successfully created.' }
        format.json { render :show, status: :created, location: @admin_photo_album }
      else
        format.html { render :new }
        format.json { render json: @admin_photo_album.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/photo_albums/1
  # PATCH/PUT /admin/photo_albums/1.json
  def update
    respond_to do |format|
      if @admin_photo_album.update(resource_params)
        format.html { redirect_to @admin_photo_album, notice: 'Photo album was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_photo_album }
      else
        format.html { render :edit }
        format.json { render json: @admin_photo_album.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/photo_albums/1
  # DELETE /admin/photo_albums/1.json
  def destroy
    @admin_photo_album.destroy
    respond_to do |format|
      format.html { redirect_to admin_photo_albums_url, notice: 'Photo album was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_photo_album
      @admin_photo_album = Admin::PhotoAlbum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resource_params
      params.require(:admin_photo_album).permit(:title, :desc, :views)
    end
end
