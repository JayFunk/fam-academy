class Admin::MastersController < AdminController
  before_action :set_admin_master, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /admin/masters
  # GET /admin/masters.json
  def index
    @admin_masters = Admin::Master.all
  end

  # GET /admin/masters/1
  # GET /admin/masters/1.json
  def show
  end

  # GET /admin/masters/new
  def new
    @admin_master = Admin::Master.new
  end

  # GET /admin/masters/1/edit
  def edit
    @images = @admin_master.images.order_by(:order => 'asc')
  end

  # POST /admin/masters
  # POST /admin/masters.json
  def create
    @admin_master = Admin::Master.new(resource_params)

    respond_to do |format|
      if @admin_master.save
        format.html { redirect_to @admin_master, notice: 'Master was successfully created.' }
        format.json { render :show, status: :created, location: @admin_master }
      else
        format.html { render :new }
        format.json { render json: @admin_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/masters/1
  # PATCH/PUT /admin/masters/1.json
  def update
    respond_to do |format|
      if @admin_master.update(resource_params)
        format.html { redirect_to @admin_master, notice: 'Master was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_master }
      else
        format.html { render :edit }
        format.json { render json: @admin_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/masters/1
  # DELETE /admin/masters/1.json
  def destroy
    @admin_master.destroy
    respond_to do |format|
      format.html { redirect_to admin_masters_url, notice: 'Master was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_master
      @admin_master = Admin::Master.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resource_params
      params.require(:admin_master).permit(:first_name, :last_name, :patronymic, :about, :profile_image)
    end
end
