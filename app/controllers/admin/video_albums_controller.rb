class Admin::VideoAlbumsController < AdminController
  before_action :set_admin_video_album, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  # GET /admin/video_albums
  # GET /admin/video_albums.json
  def index
    @admin_video_albums = Admin::VideoAlbum.all
  end

  # GET /admin/video_albums/1
  # GET /admin/video_albums/1.json
  def show
  end

  # GET /admin/video_albums/new
  def new
    @admin_video_album = Admin::VideoAlbum.new
  end

  # GET /admin/video_albums/1/edit
  def edit
    @videos = @admin_video_album.videos.order_by(:order => 'asc')
    @new_video = Video.new
  end

  # POST /admin/video_albums
  # POST /admin/video_albums.json
  def create
    @admin_video_album = Admin::VideoAlbum.new(resource_params)

    respond_to do |format|
      if @admin_video_album.save
        format.html { redirect_to @admin_video_album, notice: 'Video album was successfully created.' }
        format.json { render :show, status: :created, location: @admin_video_album }
      else
        format.html { render :new }
        format.json { render json: @admin_video_album.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/video_albums/1
  # PATCH/PUT /admin/video_albums/1.json
  def update
    respond_to do |format|
      if @admin_video_album.update(resource_params)
        format.html { redirect_to @admin_video_album, notice: 'Video album was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_video_album }
      else
        format.html { render :edit }
        format.json { render json: @admin_video_album.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/video_albums/1
  # DELETE /admin/video_albums/1.json
  def destroy
    @admin_video_album.destroy
    respond_to do |format|
      format.html { redirect_to admin_video_albums_url, notice: 'Video album was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_video_album
      @admin_video_album = Admin::VideoAlbum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resource_params
      params.require(:admin_video_album).permit(:title, :desc, :views, :main_image, :author_id)
    end
end
