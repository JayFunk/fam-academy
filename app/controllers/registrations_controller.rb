class RegistrationsController < Devise::RegistrationsController
  protected

  def after_sign_up_path_for(resource)
    finish_registration_path
  end

  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :surname])
  # end
end
