json.extract! admin_member, :id, :name, :surname, :phone, :email, :payment_method, :social_link, :expiration_date, :last_pyment_date, :total_payment, :status, :created_at, :updated_at
json.url admin_member_url(admin_member, format: :json)
