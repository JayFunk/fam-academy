json.extract! admin_article, :id, :title, :content, :author, :type, :created_at, :updated_at
json.url admin_article_url(admin_article, format: :json)
