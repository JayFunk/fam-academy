json.extract! admin_video_album, :id, :title, :desc, :views, :created_at, :updated_at
json.url admin_video_album_url(admin_video_album, format: :json)
