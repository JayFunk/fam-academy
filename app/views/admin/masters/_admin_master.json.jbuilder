json.extract! admin_master, :id, :first_name, :last_name, :patronymic, :about, :created_at, :updated_at
json.url admin_master_url(admin_master, format: :json)
