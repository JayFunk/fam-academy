json.extract! admin_photo_album, :id, :title, :desc, :views, :created_at, :updated_at
json.url admin_photo_album_url(admin_photo_album, format: :json)
