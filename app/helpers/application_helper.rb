module ApplicationHelper
  def inline_svg(name)
    file_path = "#{Rails.root}/app/assets/images/#{name}"
    return File.read(file_path).html_safe if File.exists?(file_path)
    '(not found)'
  end

  def custom_bootstrap_flash
    flash_messages = []
    flash.each do |type, message|
      type = 'success' if type == 'notice'
      type = 'error'   if type == 'alert'
      text = "<script>toastr.#{type}('#{message}');</script>"
      flash_messages << text.html_safe if message
    end
    flash_messages.join("\n").html_safe
  end
end
