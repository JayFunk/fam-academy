# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$('body').on 'click', '.change_archive', ->
  element = $(@)
  id = element.closest("tr").attr('id')
  $.ajax(
    method: 'PUT'
    url: '/api/change_archive'
    data:
      content: id
      type: 'article').done (msg) ->
        toastr.info(msg.text)
        if msg.status
          element.text 'Опубликовать'
        else
          element.text 'Добавить в архив'
