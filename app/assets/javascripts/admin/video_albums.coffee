# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

textInput = $('#videoLinkInput')
timeout = null

textInput.keyup ->
  # Clear the timeout if it has already been set.
  # This will prevent the previous task from executing
  # if it has been less than <MILLISECONDS>
  clearTimeout timeout
  # Make a new timeout set to go off in 800ms
  timeout = setTimeout((->
    link = textInput.val()
    if link.includes('<iframe')
      src = link.match(/(https|\/\/).*?"/gm)[0]
      src.substr(0, src.indexOf('"'))
      textInput.val(src)
    return
  ), 500)
  return


$("#videosList").sortable
  connectWith: ".card"
  # items: "> .image"
  cursor: "move"
  revert: true
  placeholder: "sortable-placeholder"
  tolerance: "pointer"
  start: (e, ui) ->
    ui.placeholder.width ui.item.find('.card').width()
    ui.placeholder.height ui.item.find('.card').height()
    ui.placeholder.addClass ui.item.attr('class')
    return
  update: ( event, ui ) ->
    videoOrder = $( "#videosList" ).sortable( "toArray" );
    $.ajax(
      method: 'PUT'
      url: '/api/change_order'
      data: videos: videoOrder).done (msg) ->
      toastr.success 'Порядок изменён'
      return
