# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$('body').on 'click', '.editInfo', ->
  #clean all fields
  desc = $('#descTextarea').val ''
  photographerId = $('#photographer').val ''
  photographerName = $('#photographerName').val ''

  element = $(@)
  id = element.closest(".image").attr('id')
  $('#editInfoModal').attr 'photoId', id
  $.ajax(
    method: 'GET'
    url: '/api/get_image_info'
    data: image: id).done (data) ->
      $('#descTextarea').val data.desc
      if data.author_register
        $('#photographer').val data.author
      else
        $('#photographerName').val data.author

$('body').on 'click', '#changePhotoInfo', ->
  id = $('#editInfoModal').attr 'photoId'
  desc = $('#descTextarea').val()
  photographerId = $('#photographer').val()
  photographerName = $('#photographerName').val()
  $.ajax(
    method: 'PUT'
    url: '/api/change_image_info'
    data:
      image: id
      desc: desc
      photographerId: photographerId
      photographerName: photographerName).done (msg) ->
        toastr.info 'Информация сохранена'
        $('#editInfoModal').modal 'hide'
