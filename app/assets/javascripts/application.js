// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require jquery_ujs
//= require rails-ujs
//= require popper
//= require bootstrap
//= require toastr
//= require jquery.slick

//= require backToTop

//= require jquery.lazy.min
//= require public

$(function() {
  var instance = $('.lazy').Lazy({
    scrollDirection: 'vertical',
    effect: 'fadeIn',
    visibleOnly: true,
    chainable: false,
    onError: function(element) {
        console.log('error loading ' + element.data('src'));
    }
  });
  // $('[data-toggle="tab"]').click(function (e) {
  //   setTimeout(function() {instance.update()}, 400);
  // })

  $('.card-header').click(function (e) {
    setTimeout(function() {instance.update()}, 400);
  })
  if(document.querySelector('.bricklayer')){
    var bricklayer = new Bricklayer(document.querySelector('.bricklayer'))
  }
});

$(document).ready(function() {
  $('#loader-wrapper').addClass('loaded')

  $('body').on('submit', '#contract_form', function(e){
    e.preventDefault()
    let data = {}
    $(e.target).serializeArray().forEach(function(item, i, arr) {
      data[item.name] = item.value
    })
    $.ajax({
      method: 'PUT',
      url: '/api/add_member',
      data: data
    }).done(function(res) {
      if(res.status == 'success'){
        toastr.success('Участник создан');
        localStorage.setItem('member', res.id.$oid)
        $('#payment_modal').modal('show');
      }else{
        toastr.error('Произошла ошибка');
      }
    });
  })

  $('body').on('click', '.payment_type', function(){
    let method = $(this).attr('data-method');
    $.ajax({
      method: 'PUT',
      url: '/api/change_payment_method',
      data: {
        member: localStorage.getItem('member'),
        payment_method: method
      }
    }).done(function(res) {
      if(res.status == 'success'){
        toastr.success('Способ оплаты выбран');
        if(method == 'card'){
          $("#card_payment_alert").addClass("in")
        }
      }else{
        toastr.error('Произошла ошибка');
      }
    });
  })
})
