#= require jquery3
#= require jquery-ui
#= require rails-ujs
#= require popper
#= require bootstrap
#= require dropzone
#= require toastr

#= require redactor.min.js
#= require redactor/counter.min.js
#= require redactor/fullscreen.min.js
#= require redactor/imagemanager.min.js
#= require redactor/table.min.js
#= require redactor/video.min.js
#= require redactor/ru.js


#= require_tree ./admin

toastr.options.progressBar = true;

$('.redactor').redactor
  lang: 'ru'
  plugins: ['fullscreen', 'counter', 'imagemanager', 'table', 'video']
  imageUpload: '/api/upload_image'
  imageData:
    origin:'redactor',
    authenticity_token: $('[name="authenticity_token"]').val()


# $('.redactor').froalaEditor
#   language: 'ru'
#   imageUploadURL: '/api/upload_image/'
#   imageUploadParams:
#     origin: 'redactor'
#     authenticity_token: $('[name="authenticity_token"]').val()
#   paragraphFormat:
#     N: 'Нормально'
#     H2: 'Заголовок 2'


# $('.dropzone').dropzone
#   url: '/api/upload_image'
#   dictDefaultMessage: 'Перетащите файлы сюда или нажмите'
#   complete: ->
#     toastr.success 'Изображения сохранены'
#     return
#   error: ->
#     toastr.danger 'Ошибка при сохранении'
#     return

$("#sortableGrid").sortable
  connectWith: ".card"
  # items: "> .image"
  cursor: "move"
  revert: true
  placeholder: "sortable-placeholder"
  tolerance: "pointer"
  start: (e, ui) ->
    ui.placeholder.width ui.item.find('.card').width()
    ui.placeholder.height ui.item.find('.card').height()
    ui.placeholder.addClass ui.item.attr('class')
    return
  update: ( event, ui ) ->
    imagesOrder = $( "#sortableGrid" ).sortable( "toArray" );
    $.ajax(
      method: 'PUT'
      url: '/api/change_order'
      data: images: imagesOrder).done (msg) ->
      toastr.success 'Порядок изменён'
      return


$("#sortableGrid").disableSelection


$('body').on 'click', '.setMainImage', ->
  element = $(@)
  id = element.closest(".image").attr('id')
  $.ajax(
    method: 'PUT'
    url: '/api/main_image'
    data: image: id).done (msg) ->
    toastr.info 'Главная картинка изменена'
    $('.setMainImage').removeClass 'd-none'
    $('.card').removeClass 'border-primary'
    console.log 'Hide'
    setTimeout (->
      element.addClass 'd-none'
      element.closest('.card').addClass 'border-primary'
      console.log 'Show'
      return
    ), 1

$('body').on 'click', '.deleteImage', ->
  if confirm 'Точно удалить?'
    element = $(@)
    id = element.closest(".image").attr('id')
    $.ajax(
      method: 'DELETE'
      url: '/api/delete_image'
      data: image: id).done (msg) ->
      toastr.info 'Картинка удалена'
      element.closest(".image").remove()

$('body').on 'click', '.archiveImage', ->
  element = $(@)
  image = element.closest(".image")
  textConfirm = ""
  textToastr = ""
  archived = false
  if image.hasClass 'archived'
    archived = true
    textConfirm = 'Достать из архива?'
    textToastr = "Картинка вытащена из архива"
  else
    textConfirm = 'Добавить в архив?'
    textToastr = "Картинка перенесена в архив"
  if confirm textConfirm
    id = image.attr('id')
    $.ajax(
      method: 'PUT'
      url: '/api/archive_image'
      data: image: id).done (msg) ->
        if msg.status == 'main_image'
          toastr.error msg.text
        else
          toastr.info textToastr
          if archived
            image.removeClass 'archived'
            element.removeClass 'text-warning'
            element.addClass 'text-secondary'
          else
            image.addClass 'archived'
            element.removeClass 'text-secondary'
            element.addClass 'text-warning'

$('body').on 'click', '.add_pyment_btn', ->
  id = $(@).attr 'member'
  $('#add_payment_modal #member_input').val id

$('body').on 'submit', '#payment_form', (e) ->
  e.preventDefault()
  data = {}
  $(e.target).serializeArray().forEach (item, i, arr) ->
    data[item.name] = item.value

  $.ajax(
    method: 'PUT'
    url: '/api/create_payment'
    data: data).done (msg) ->
    toastr.info 'Взнос добавлен'
