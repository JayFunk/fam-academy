# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$('[data-toggle="tooltip"]').tooltip()

$(document).ready ->
  $('.activityCarousel').slick
    nextArrow: '<button type="button" class="next-slide-btn"><i class="fas fa-angle-right"></i></button>',
    prevArrow: '<button type="button" class="prev-slide-btn"><i class="fas fa-angle-left"></i></button>'
    # dots: true
    # lazyLoad: 'progressive'
    infinite: true
    speed: 400
    slidesToShow: 3
    slidesToScroll: 3
    autoplay: true
    autoplaySpeed: 7000
    responsive: [
      {
        breakpoint: 992
        settings:
          slidesToShow: 2
          slidesToScroll: 2
      }
      {
        breakpoint: 600
        settings:
          slidesToShow: 1
          slidesToScroll: 1
      }
    ]
  $('.gallery__carousel').slick
    nextArrow: '<button type="button" class="next-slide-btn"><i class="fas fa-angle-right"></i></button>',
    prevArrow: '<button type="button" class="prev-slide-btn"><i class="fas fa-angle-left"></i></button>'
    dots: true
    infinite: true
    speed: 400
    slidesToShow: 1
    # adaptiveHeight: true
    autoplay: true
    autoplaySpeed: 7000
    # lazyLoad: 'progressive'

  $('.events__carousel').slick
    nextArrow: '<button type="button" class="next-slide-btn"><i class="fas fa-angle-right"></i></button>',
    prevArrow: '<button type="button" class="prev-slide-btn"><i class="fas fa-angle-left"></i></button>'
    dots: false
    infinite: true
    speed: 400
    slidesToShow: 1
    # adaptiveHeight: true
    autoplay: true
    autoplaySpeed: 20000
    # lazyLoad: 'progressive'


  $('.reviews__carousel').slick
    nextArrow: '<button type="button" class="next-slide-btn"><i class="fas fa-angle-right"></i></button>',
    prevArrow: '<button type="button" class="prev-slide-btn"><i class="fas fa-angle-left"></i></button>'
    dots: false
    infinite: true
    speed: 400
    slidesToShow: 1
    adaptiveHeight: true
    autoplay: true
    autoplaySpeed: 25000
    # lazyLoad: 'progressive'
