class Admin::Article
  include Mongoid::Document
  include Mongoid::Slug
  include Mongoid::Timestamps
  field :title, type: String
  field :content, type: String
  field :desc, type: String
  field :type, type: String
  field :archive, type: Boolean, default: true
  mount_uploader :header, ImageUploader

  belongs_to :author, class_name: "User", optional: true
  slug :title
end
