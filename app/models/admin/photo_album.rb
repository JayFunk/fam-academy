class Admin::PhotoAlbum
  include Mongoid::Document
  include Mongoid::Slug
  include Mongoid::Timestamps
  field :title, type: String
  field :desc, type: String
  field :views, type: Integer
  field :archive, type: Boolean, default: false

  belongs_to :author, class_name: "User", optional: true
  has_many :images, class_name: "Image", :dependent => :destroy

  slug :title
end
