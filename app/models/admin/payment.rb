class Admin::Payment
  include Mongoid::Document
  field :sum, type: Integer
  field :method, type: String
  field :date, type: Date
  field :type, type: String

  belongs_to :member, class_name: 'Admin::Member', optional: true

  default_scope { order(date: :desc) }

  def get_payment_type
    if self.method == 'entrance'
      'Вступительный'
    elsif self.method == 'membership'
      'Членский'
    else
      'Не выбран или не известен'
    end
  end

  def get_payment_method_type
    if self.method == 'card'
      'Карта'
    elsif self.method == 'skies'
      'SKIES'
    elsif self.method == 'cash'
      'Наличные'
    else
      'Не выбран или не известен'
    end
  end
end
