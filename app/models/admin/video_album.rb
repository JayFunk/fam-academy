class Admin::VideoAlbum
  include Mongoid::Document
  include Mongoid::Slug
  include Mongoid::Timestamps
  field :title, type: String
  field :desc, type: String
  field :views, type: Integer
  field :archive, type: Boolean, default: false
  mount_uploader :main_image, ImageUploader

  belongs_to :author, class_name: "User", optional: true
  has_many :videos, class_name: "Video", :dependent => :destroy

  slug :title
end
