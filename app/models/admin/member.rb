class Admin::Member
  include Mongoid::Document
  include Mongoid::Timestamps
  # Personal information
  field :first_name, type: String
  field :last_name, type: String
  field :patronymic, type: String

  field :phone, type: Integer
  field :email, type: String
  field :birthday, type: Date
  field :passport_series, type: Integer
  field :passport_number, type: Integer
  field :passport_issued, type: String
  field :address, type: String
  field :social_link, type: String

  # Payment information
  field :payment_method, type: String  # 'card' or 'skies'
  field :expiration_date, type: Date
  field :last_pyment_date, type: Date
  field :total_payment, type: Integer, default: 0
  field :status, type: String, default: 'inactive' # 'active', 'ending' or 'inactive'

  has_many :payments, class_name: 'Admin::Payment'

  default_scope { order(created_at: :desc) }

  def get_localize_status
    if self.status == 'active'
      'Активный'
    elsif self.status == 'ending'
      'Заканчивается'
    else
      'Не активный'
    end
  end

  def get_payment_method_type
    if self.payment_method == 'card'
      'Карта'
    elsif self.payment_method == 'skies'
      'SKIES'
    else
      'Не выбран или не известен'
    end
  end

  def total_payment
    total = 0
    self.payments.each do |payment|
      total += payment.sum
    end
    total
  end

  def last_payment
    self.payments.order(date: :desc).first
  end

  # def set_status
  #   if self.last_payment.date
  #     if Time.now >= self.last_payment.date + 1.month
  #       self.status = 'inactive'
  #     else
  #       self.status = 'active'
  #     end
  #   else
  #     self.status = 'inactive'
  #   end
  # end
end
