class Admin::Master
  include Mongoid::Document
  # include Mongoid::Slug
  mount_uploader :profile_image, ImageUploader

  field :first_name, type: String
  field :last_name, type: String
  field :patronymic, type: String

  field :about, type: String
  has_many :images, class_name: "Image", :dependent => :destroy
end
