class User
  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :trackable, :validatable

  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable

  devise :omniauthable, omniauth_providers: %i[vkontakte]

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time



  field :name, type: String
  field :surname, type: String

  field :role, type: String, default: "user"
  field :accepted, type: Boolean, default: false
  field :writer, type: Boolean, default: false
  field :photographer, type: Boolean, default: false


  has_many :articles, class_name: "Admin::Article"
  has_many :photo_albums, class_name: "Admin::PhotoAlbum"
  has_many :video_albums, class_name: "Admin::VideoAlbum"


  field :provider
  field :uid
  field :fakeEmail, type: Boolean, default: false

  def self.find_for_oauth(auth)
    user = User.where(uid: auth.uid, provider: auth.provider).first

    unless user
      user = User.create(
        uid:      auth.uid,
        provider: auth.provider,
        email:    User.dummy_email(auth),
        password: Devise.friendly_token[0, 20],
        fakeEmail: true
      )
    end

    user
  end

  def will_save_change_to_email?
    false
  end

  # def self.new_with_session(params, session)
  #  super.tap do |user|
  #    if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
  #      user.email = data["email"] if user.email.blank?
  #    end
  #  end
  # end

  private

  def self.dummy_email(auth)
    "#{auth.uid}-#{auth.provider}@fam-academy.ru"
  end
end
