class Image
  include Mongoid::Document
  field :order, type: Integer
  field :type
  field :main_image, type: Boolean, default: false
  field :bg_image, type: Boolean, default: false
  field :archive, type: Boolean, default: false
  field :desc
  field :author
  field :author_register, type: Boolean, default: false

  mount_uploader :image, ImageUploader
  belongs_to :photo_album, class_name: "Admin::PhotoAlbum", optional: true
  belongs_to :master, class_name: "Admin::Master", optional: true
end
