class Video
  include Mongoid::Document
  field :title, type: String
  field :order, type: Integer
  field :link, type: String
  field :archive, type: Boolean, default: false

  mount_uploader :cover_image, ImageUploader
  belongs_to :video_album, class_name: "Admin::VideoAlbum", optional: true
end
